import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { NotFoundGecko } from "../images/notFoundGecko"

const Multiply = () => (
  <Layout>
    <SEO title="Division" />
    <h2>Próximamente: División...</h2>
    <h2>Coming soon...</h2>
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        height: "60vh",
      }}
    >
      <NotFoundGecko />
    </div>
  </Layout>
)

export default Multiply
