import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { TimesGecko } from "../images/timesGecko"
import { randomInsectFacts } from "../components/facts"
import { AnswerInput, AnswerInputWrapper } from "../components/answer"
import { TalkBubble } from "../components/chatBubble"
import { digitRandom } from "../utils/utils"

const easy = [0, 1, 2, 10]
const intermediate = [3, 4, 5]
const hard = [6, 7, 8, 9]
function numberFromSet(set) {
  var rndm = Math.floor(Math.random() * set.length)
  return set[rndm]
}

const answerObject = {
  ones: "",
  tens: "",
  hundreds: "",
}

const Restas = () => {
  const [answer, setAnswer] = React.useState(answerObject)
  const [success, setSuccess] = React.useState(false)
  const [message, setMessage] = React.useState(null)
  const [level, setLevel] = React.useState(easy)
  const [multiplier, setMultiplier] = React.useState(numberFromSet(level))
  const [number, setNumber] = React.useState(digitRandom(10))

  const answerInput = React.useRef()
  const newQuestion = React.useRef()

  const updateAnswer = e => {
    setAnswer({ ...answer, [e.target.name]: e.target.value })
  }

  const ints = val => (val ? val : "0")
  const handleSubmit = event => {
    event.preventDefault()
    const concatAnswers =
      ints(answer.hundreds) + ints(answer.tens) + ints(answer.ones)
    const finalAnswer = parseInt(concatAnswers)

    if (number * multiplier === finalAnswer) {
      setMessage("Correcto!!! " + randomInsectFacts())
      setSuccess(true)
      newQuestion.current.focus()
    } else {
      setMessage("Trata de nuevo, algo falta...")
      setSuccess(false)
      answerInput.current.focus()
    }
  }

  const resetForm = () => {
    setAnswer(answerObject)
    setMessage(null)
    setNumber(digitRandom(10))
    setMultiplier(numberFromSet(level))
    setSuccess(false)
    answerInput.current.focus()
  }

  const changeMultiplier = value => {
    if (value !== level) {
      setAnswer(answerObject)
      setLevel(value)
      setMultiplier(numberFromSet(value))

      setSuccess(false)

      setMessage(null)
      setNumber(digitRandom(10))
    }
    answerInput.current.focus()
  }
  return (
    <Layout>
      <SEO title="Multiplicacion" />
      <h2>Tabla de multiplicación del {multiplier}</h2>

      <TalkBubble message={message} success={success} />

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div>
          <TimesGecko />
        </div>
        <form>
          <div>
            <button
              type="reset"
              ref={newQuestion}
              onClick={() => resetForm()}
              style={success ? { background: "#F86903", color: "#fff" } : {}}
            >
              Nueva Pregunta
            </button>
          </div>
          <div className="equation-wrapper">
            <div className="numbers">
              <div>{number}</div>
              <div>x {multiplier}</div>
            </div>
            <AnswerInputWrapper>
              <AnswerInput
                type="tel"
                name="hundreds"
                maxLength="1"
                value={answer.hundreds}
                onChange={updateAnswer}
              />

              <AnswerInput
                type="tel"
                name="tens"
                maxLength="1"
                value={answer.tens}
                onChange={updateAnswer}
              />
              <AnswerInput
                type="tel"
                name="ones"
                value={answer.ones}
                onChange={updateAnswer}
                maxLength="1"
                ref={answerInput}
              />
            </AnswerInputWrapper>

            <div>
              <button
                type="submit"
                onClick={handleSubmit}
                className="submit"
                disabled={success}
              >
                Responder >
              </button>
            </div>
          </div>
        </form>
      </div>
      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
          marginTop: "10px",
        }}
      >
        <button
          type="reset"
          className={level === easy ? "selected" : ""}
          onClick={() => changeMultiplier(easy)}
        >
          Tablas: 0, 1, 2, 10
        </button>
        <button
          type="reset"
          className={level === intermediate ? "selected" : ""}
          onClick={() => changeMultiplier(intermediate)}
        >
          Intermedio: 3, 4, 5
        </button>
        <button
          type="reset"
          className={level === hard ? "selected" : ""}
          onClick={() => changeMultiplier(hard)}
        >
          Avanzado: 6, 7, 8, 9
        </button>
      </div>
    </Layout>
  )
}

export default Restas
