import React from "react"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { RestasGecko } from "../images/restasGecko"
import { randomInsectFacts } from "../components/facts"
import { AnswerInput, AnswerInputWrapper } from "../components/answer"
import { TalkBubble } from "../components/chatBubble"
import { digitRandom } from "../utils/utils"

const answerObject = {
  ones: "",
  tens: "",
  hundreds: "",
}

const Restas = () => {
  const [answer, setAnswer] = React.useState(answerObject)
  const [success, setSuccess] = React.useState(false)
  const [message, setMessage] = React.useState(null)
  const [digits, setDigits] = React.useState(2)
  const [number1, setNumber1] = React.useState(digitRandom(digits))
  const [number2, setNumber2] = React.useState(digitRandom(digits))

  const answerInput = React.useRef()
  const newQuestion = React.useRef()

  const updateAnswer = e => {
    setAnswer({ ...answer, [e.target.name]: e.target.value })
  }

  const ints = val => (val ? val : "0")
  const handleSubmit = event => {
    event.preventDefault()
    const concatAnswers =
      ints(answer.thousands) +
      ints(answer.hundreds) +
      ints(answer.tens) +
      ints(answer.ones)
    const finalAnswer = parseInt(concatAnswers)

    let large = number1
    let small = number2
    if (number1 < number2) {
      large = number2
      small = number1
    }
    if (large - small === finalAnswer) {
      setMessage("Correcto!!! " + randomInsectFacts())
      setSuccess(true)
      newQuestion.current.focus()
    } else {
      setMessage("Trata de nuevo, algo falta...")
      setSuccess(false)
      answerInput.current.focus()
    }
  }

  const resetForm = () => {
    setAnswer(answerObject)
    setMessage(null)
    setNumber1(digitRandom(digits))
    setNumber2(digitRandom(digits))
    setSuccess(false)
    answerInput.current.focus()
  }

  const changeDigits = value => {
    if (value !== digits) {
      setAnswer(answerObject)
      setDigits(value)
      setSuccess(false)

      setMessage(null)
      setNumber1(digitRandom(value))
      setNumber2(digitRandom(value))
    }
    answerInput.current.focus()
  }
  return (
    <Layout>
      <SEO title="Restas" />
      <h2>
        Restas de {digits} dígito{digits > 1 && "s"}
      </h2>

      <TalkBubble message={message} success={success} />

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <div>
          <RestasGecko />
          <div>
            <button type="reset" onClick={() => changeDigits(1)}>
              1 dígito
            </button>
            <button type="reset" onClick={() => changeDigits(2)}>
              2 dígitos
            </button>
            <button type="reset" onClick={() => changeDigits(3)}>
              3 dígitos
            </button>
          </div>
        </div>
        <form>
          <div>
            <button
              type="reset"
              ref={newQuestion}
              onClick={() => resetForm()}
              style={success ? { background: "#F86903", color: "#fff" } : {}}
            >
              Nueva Resta
            </button>
          </div>
          <div className="equation-wrapper">
            <div className="numbers">
              <div>{number1 >= number2 ? number1 : number2}</div>
              <div>- {number2 <= number1 ? number2 : number1}</div>
            </div>
            <AnswerInputWrapper>
              <AnswerInput
                type="tel"
                name="hundreds"
                maxLength="1"
                value={answer.hundreds}
                onChange={updateAnswer}
              />

              <AnswerInput
                type="tel"
                name="tens"
                maxLength="1"
                value={answer.tens}
                onChange={updateAnswer}
              />
              <AnswerInput
                type="tel"
                name="ones"
                value={answer.ones}
                onChange={updateAnswer}
                maxLength="1"
                ref={answerInput}
              />
            </AnswerInputWrapper>

            <div>
              <button
                type="submit"
                onClick={handleSubmit}
                className="submit"
                disabled={success}
              >
                Responder >
              </button>
            </div>
          </div>
        </form>
      </div>
    </Layout>
  )
}

export default Restas
