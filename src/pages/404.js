import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { NotFoundGecko } from "../images/notFoundGecko"

const NotFoundPage = () => (
  <Layout>
    <SEO title="404: Not found" />
    <h1>NOT FOUND</h1>
    <p>Encontraste algo que no existe, que triste...</p>
    <div
      style={{
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        height: "60vh",
      }}
    >
      <NotFoundGecko />
    </div>
  </Layout>
)

export default NotFoundPage
