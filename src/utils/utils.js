// Generates a random 1, 2 or 3 digit number
export function digitRandom(digit) {
  let min = 10
  let max = 99
  if (digit === 3) {
    min = 100
    max = 999
  }
  if (digit === 1) {
    min = 0
    max = 9
  }

  if (digit === 10) {
    min = 0
    max = 10
  }

  return Math.floor(Math.random() * (max - min + 1) + min)
}
