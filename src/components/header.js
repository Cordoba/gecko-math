import PropTypes from "prop-types"
import React from "react"
import { colors, widths } from "./theme"
import styled from "styled-components"

const HeaderStyle = styled.header`
  background: ${colors.orange};
  margin-bottom: 10px;
`

const TitleWrapper = styled.div`
  margin: 0 auto;
  max-width: ${widths.max};
  padding: 12px;
  h1 {
    color: #fff;
    margin: 0;
  }
`

const Header = ({ siteTitle }) => (
  <HeaderStyle>
    <TitleWrapper>
      <h1>{siteTitle}</h1>
    </TitleWrapper>
  </HeaderStyle>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
