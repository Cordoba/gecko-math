import React from "react"
import styled, { keyframes } from "styled-components"
import { colors } from "./theme"

const slideInFromLeft = keyframes`
  0% {
    transform: translateX(-100%);
  }
  100% {
    transform: translateX(0);
  }
`

const BubbleStyle = styled.div`
  animation: ${slideInFromLeft} 1s ease-out 0s 1;
  margin: 0 auto;
  padding: 0 10px;
  display: block;
  position: relative;
  bottom: -40px;
  width: 80%;
  height: auto;
  background-color: ${props => (props.success ? "green" : "red")};
  color: #fff;
  border-radius: 30px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border: 6px solid ${colors.darkBrown};
  &:before {
    content: " ";
    position: absolute;
    width: 0;
    height: 0;
    left: 30px;
    right: auto;
    top: auto;
    bottom: -40px;
    border: 20px solid;
    border-color: ${colors.darkBrown} transparent transparent
      ${colors.darkBrown};
  }
  &:after {
    content: " ";
    position: absolute;
    width: 0;
    height: 0;
    left: 38px;
    right: auto;
    top: auto;
    bottom: -20px;
    border: 12px solid;
    border-color: ${props =>
      props.success
        ? "green transparent transparent green"
        : "red transparent transparent red"};
  }
`

export const TalkBubble = ({ message, success }) => {
  return (
    <div style={{ height: "160px" }}>
      {message && (
        <BubbleStyle success={success}>
          <p>{message}</p>
        </BubbleStyle>
      )}
    </div>
  )
}
