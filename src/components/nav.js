import React from "react"
import { Link } from "gatsby"
import styled from "styled-components"
import { widths, colors } from "./theme"

const MainNav = styled.nav`
  margin: 0 auto;
  bottom: 10px;
  width: 100%;
  max-width: ${widths.max};
  display: flex;
  flex-direction: row;
  justify-content: space-around;

  a {
    color: ${colors.brown};
    background: #fceccd;
    text-decoration: none;
    font-size: 16px;
    span:first-child {
      font-size: 50px;
    }
    border: 2px solid ${colors.darkBrown};
    display: flex;
    flex-direction: column;
    align-items: center;
    border-radius: 12px;
    padding: 0 20px;
    @media (max-width: 767px) {
      padding: 0 8px;
    }
    &.active {
      color: ${colors.orange};
      background-color: #fff;
    }
  }
`

export const Nav = () => {
  return (
    <MainNav>
      <Link to="/" activeClassName="active">
        <span> + </span>
        <span>Sumas</span>
      </Link>
      <Link to="/restas/" activeClassName="active">
        <span> - </span>
        <span>Restas</span>
      </Link>
      <Link to="/division/" activeClassName="active">
        <span dangerouslySetInnerHTML={{ __html: "&divide" }} />
        <span>División</span>
      </Link>
      <Link to="/multiplicacion/" activeClassName="active">
        <span> x </span>
        <span>Multiplicación</span>
      </Link>
    </MainNav>
  )
}
