export const colors = {
  orange: "#F86903",
  darkBrown: "#311414",
  brown: "#54342B",
}

export const widths = {
  max: "960px",
}
