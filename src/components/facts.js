const insectFacts = [
  "Los geckos comen insectos!",
  "Los insectos tienen 6 patas",
  "Los insectos tiene 3 partes del cuerpo: cabeza, torax y adbomen",
  "Muchos insectos voladores tienen 2 pares (4) alas",
  "Las arañas no son insectos porque tienen 8 patas. No 6",
  "La mariquita es un insecto",
  "Los insectos son animales invertebrados",
  "Hay alrededor de 1 millón de especies descubiertas de insectos!",
  "Las antenas son otra característica de los insectos",
  "En un hormiguero pueden vivir mas de 20 millones de hormigas!",
]

function randomFacts(factArray) {
  return factArray[Math.floor(Math.random() * factArray.length)]
}

export const randomInsectFacts = () => randomFacts(insectFacts)
