import styled, { keyframes } from "styled-components"
import { colors } from "./theme"

export const AnswerInputWrapper = styled.div`
  width: 100%;
  padding: 10px 14px;
`

const focusBorder = keyframes`
  0% {
    opacity: 1;
  }
  50% {
    opacity: .3;
  }
`
export const AnswerInput = styled.input`
  border: 2px dashed #999;
  font-size: 58px;
  padding: 0 2px 0 0;
  width: 40px;
  color: ${colors.brown};
  text-align: right;
  & + input {
    margin-left: 4px;
  }
  &:focus {
    animation: ${focusBorder} 2s infinite;
    border-color: ${colors.orange};
    caret-color: ${colors.orange};
  }
`
